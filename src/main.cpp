#include <stdio.h>
#include <string.h>

#include "stm32f10x_usart.h"
#include "stm32f10x_spi.h"
#include "Spi.h"
#include "bluepill.h"
#include "millis.h"
#include "micros.h"
#include "serial.h"
#include "led.h"

#include "RC522.h"

Serial serial2(USART2);
Led led(PC13);

RC522 rfid_reader(SPI2, PB12, PA8, PA9);

uint8_t CardID[5];
char bufferRFID[30];
uint8_t MyID[5] = {0x82, 0x1f, 0x2f, 0x00, 0xb2};	//My card on my keys


void main() {

	millis_init();
	micros_init();

	led.turn_off();

	led.turn_on();


	serial2.send("\n\n --- reset -- \n\n");



//	Spi my_spi(SPI2, PB12);
//
//	while(1) {
//		my_spi.write_byte(0xAA);
//		delay(100);
//		led.toggle();
//	}


	//serial2.send_char(getFirmwareVersion() + '0');
	//serial2.send("\n\n");

//	Spi spi2(SPI2);
//	RC522_CS.digital_write(0);
//
//	while(1) {
//		spi2.write_byte(((MFRC522_REG_VERSION << 1) & 0x7E) | 0x80);
//		char b = spi2.read_byte() + '0';
//		serial2.send_char(b);
//		delay(100);
//	}
//
//
//	while(1) {
//		spi2.write_byte(0xAA);
//		delay(1);
//		led.toggle();
//	}


	while (1) {
		//Delay(10);
		/* Toggle pins */
		//GPIO_WriteBit(GPIOC, GPIO_Pin_3, (BitAction)(1 - GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_3)));
		//GPIO_WriteBit(GPIOC, GPIO_Pin_4, (BitAction)(1 - GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_4)));
		//GPIO_WriteBit(GPIOC, GPIO_Pin_5, (BitAction)(1 - GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_5)));
		//-----------------------------RFID Analayzer------------------------------------------


		if (TM_MFRC522_Check(CardID) == MI_OK)
		{
		  led.toggle();
			sprintf(bufferRFID, "[%02x-%02x-%02x-%02x-%02x]\n", CardID[0], CardID[1], CardID[2], CardID[3], CardID[4]);
			serial2.send(bufferRFID);
			sprintf(bufferRFID, "0x%x\n", rfid_reader.get_firmware_version());
			serial2.send(bufferRFID);

			//Send_string_uart("\n\r");
			//Check if this is my card
		  if (TM_MFRC522_Compare(CardID, MyID) == MI_OK)
			{
				//Send_string_uart("(^_^) Perfect!\n\r");
				//sprintf(bufferRFID, "[%s voi Card ID=%02x-%02x-%02x-%02x-%02x]", dataUDP_permit, CardID[0], CardID[1], CardID[2], CardID[3], CardID[4]);
				//send_udp_data(UDP_pactkage_sample,bufferRFID,strlen(bufferRFID),myudpport);
				//send_udp_data(buf,dataUDP_permit,strlen(dataUDP_permit),myudpport);

			}
			else
			{
				//Send_string_uart("(0_0) Bad!\n\r");
				//sprintf(bufferRFID, "[%s voi Card ID=%02x-%02x-%02x-%02x-%02x]", dataUDP_deny, CardID[0], CardID[1], CardID[2], CardID[3], CardID[4]);
			  //make_udp_reply_from_request(buf,str,strlen(str),myudpport);
				//send_udp_data(UDP_pactkage_sample,bufferRFID,strlen(bufferRFID),myudpport);
			}

		}
		//Send_string_uart("Waiting for RFID Card...!\n\r");

		delay(10);

	} //while(1)

} //main
